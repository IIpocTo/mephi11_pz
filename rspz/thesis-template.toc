\select@language {russian}
\contentsline {chapter}{Введение}{6}{chapter*.3}
\contentsline {chapter}{\numberline {1}Изучение формализации ООП в программном средстве TOOP}{8}{chapter.1}
\contentsline {section}{\numberline {1.1}Общие положения теории объектов}{8}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Свойства объектно-ориентированного подхода}{8}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Механизм повторного использования}{9}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Понятия ковариантности, контравариантности и инвариантности}{10}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Основные понятия сигма-алгебры}{11}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Функции как объекты}{13}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Аргументы со значениями по умолчанию}{13}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Многоместные функции с именованными аргументами}{14}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Описание выбора системы управления репозиториями с кодом}{14}{section.1.3}
\contentsline {section}{\numberline {1.4}Выводы}{15}{section.1.4}
\contentsline {section}{\numberline {1.5}Цели и задачи НИР}{16}{section.1.5}
\contentsline {chapter}{\numberline {2}Разработка синтаксиса типизированного объектно-ориентированного языка системы TOOP}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Описание синтаксиса типизированного объектно-ориентированного языка системы TOOP}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Описание формальной системы типизации}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Типизация объектов}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Типизация функций}{17}{subsection.2.2.2}
\contentsline {chapter}{\numberline {3}Результаты проектирования усовершенствованного интерпретатора с поддержкой выбора уровня типизации}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Выбор модели жизненного цикла}{18}{section.3.1}
\contentsline {section}{\numberline {3.2}Выбор методологии проектирования}{18}{section.3.2}
\contentsline {section}{\numberline {3.3}Описание разработанной схемы интерпретатора}{18}{section.3.3}
\contentsline {chapter}{\numberline {4}Программная реализация спроектированного интерпретатора с поддержкой выбора уровня типизации}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Описание Play Framework}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Инъекция зависимостей}{19}{section.4.2}
\contentsline {section}{\numberline {4.3}Спецификация кода}{19}{section.4.3}
\contentsline {section}{\numberline {4.4}Встраивание разработанного интерпретатора}{19}{section.4.4}
\contentsline {section}{\numberline {4.5}Тестирование кода}{19}{section.4.5}
\contentsline {chapter}{Заключение}{21}{chapter*.4}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {english}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
\select@language {russian}
